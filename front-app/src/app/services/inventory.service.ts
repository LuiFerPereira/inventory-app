
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class InventoryService {

  private baseUrl = 'http://localhost:8080/api/inventory/product/';

  constructor(public httpClient: HttpClient) {}

  searchProduct( code?: string ) {
    if (code) {
      return this.httpClient.get(`${this.baseUrl}${code}`);
    } else {
      return this.httpClient.get(this.baseUrl);
    }
  }

  uploadFile(email: string, file: File) {
    const HttpUploadOptions = {
      headers: new HttpHeaders({ responseType: 'text'})
    };
    const formdata: FormData = new FormData();
    formdata.append('file', file);

    const req = new HttpRequest('POST', `${this.baseUrl}upload?email=${email}`, formdata, {
      reportProgress: true,
      responseType: 'text'
    });

    return this.httpClient.request(req);
  }

  deleteProduct(code: string) {
    return this.httpClient.delete(`${this.baseUrl}${code}`);
  }

  patchProduct(code: string, amount: number, cost: number) {
    return this.httpClient.patch(`${this.baseUrl}${code}/${amount}/${cost}`, {});
  }

}


export interface Product {
  id: number;
  code: string;
  productName: string;
  productType: number;
  availabilityDate: string;
  amount: number;
  cost: number;
  price: number;
}
