import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { InventoryService, Product } from 'src/app/services/inventory.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html'
})
export class ProductDetailsComponent implements OnInit, OnDestroy {

  private code: string;
  private sub: any;
  private inventory: Product[];
  constructor(private inventoryService: InventoryService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
        this.code = params.code;
    });
    this.searchProduct(this.code);
  }

  ngOnDestroy() {
      this.sub.unsubscribe();
  }

  searchProduct(code: string) {
    this.inventoryService.searchProduct(code).subscribe((res: any) => {
        this.inventory = [];
        this.inventory.push(res);
    }, (err) => {
      this.inventory = [];
    } );
  }
}
