import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { InventoryService, Product } from 'src/app/services/inventory.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html'
})
export class UploadComponent {

  @ViewChild('labelImport')
  labelImport: ElementRef;

  private imported = false;
  private errors = false;
  formImport: FormGroup;
  fileToUpload: File = null;

  constructor(private inventoryService: InventoryService) {
    this.formImport = new FormGroup({
      inputEmail: new FormControl('', [Validators.required, Validators.email]),
      importFile: new FormControl('', Validators.required)
    });
  }

  onFileChange(files: FileList) {
    this.labelImport.nativeElement.innerText = Array.from(files)
      .map(f => f.name)
      .join(', ');
    this.fileToUpload = files.item(0);
  }

  import(): void {
    this.inventoryService.uploadFile(this.formImport.controls.inputEmail.value
      , this.fileToUpload).subscribe( (res: any) => {
        this.imported = true;
        this.errors = false;
    }, (err) => {
      this.imported = true;
      this.errors = true;
    });
  }
}
