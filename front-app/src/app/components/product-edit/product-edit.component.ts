import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { InventoryService, Product } from 'src/app/services/inventory.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html'
})
export class ProductEditComponent implements OnInit, OnDestroy {

  @ViewChild('inputCode')
  inputCode: ElementRef;
  @ViewChild('inputAmount')
  inputAmount: ElementRef;
  @ViewChild('inputCost')
  inputCost: ElementRef;

  private code: string;
  private edited = false;
  private errors = false;
  private sub: any;
  private inventory: Product[];
  constructor(private inventoryService: InventoryService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
        this.code = params.code;
    });
    this.searchProduct(this.code);
  }

  ngOnDestroy() {
      this.sub.unsubscribe();
  }

  searchProduct(code: string) {
    this.inventoryService.searchProduct(code).subscribe((res: any) => {
        this.inventory = [];
        this.inventory.push(res);
    }, (err) => {
      this.inventory = [];
    } );
  }

  editProduct() {
    this.inventoryService.patchProduct(this.inputCode.nativeElement.value
      , +this.inputAmount.nativeElement.value
      , +this.inputCost.nativeElement.value).subscribe( (res: any) => {
          this.edited = true;
          this.errors = false;
      }, (err) => {
        this.edited = true;
        this.errors = true;
      });
  }
}
