import { Component, OnInit } from '@angular/core';
import { InventoryService, Product } from 'src/app/services/inventory.service';


@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html'
})
export class InventoryComponent implements OnInit {

  public inventory: Product[] = [];

  constructor(private inventoryService: InventoryService) {
  }

  ngOnInit() {
    this.searchProduct();
  }

  searchProduct(code?: string ) {
    this.inventoryService.searchProduct(code).subscribe((res: any) => {
      if (res.content) {
        this.inventory = res.content;
      } else {
        this.inventory = [];
        this.inventory.push(res);
      }
    }, (err) => {
      this.inventory = [];
    } );
  }

  deleteProduct(id: number, code: string) {
    if (confirm('Are you sure to delete product with code: ' + code)) {
      this.inventoryService.deleteProduct(code).subscribe( () => {
        this.searchProduct();
      });
    }
  }
}
